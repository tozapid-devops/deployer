#Download base image ubuntu
FROM ubuntu:24.04

ENV DEBIAN_FRONTEND noninteractive

# Update Ubuntu Software repository
RUN apt-get -qy update
RUN apt-get install -qy --no-install-recommends apt-utils
RUN apt-get install -qy curl
RUN apt-get install -qy unzip gzip
RUN apt-get install -qy dialog
RUN apt-get install -qy python3 git
RUN apt-get install -qy software-properties-common
RUN apt-add-repository --yes --update ppa:ansible/ansible
RUN apt-get install -qy ansible ansible-lint

# aws
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && unzip awscliv2.zip
RUN ./aws/install && aws --version
# /aws

CMD ["/bin/bash"]
